Clockwise Snailshell Pattern Implementation From Two Dimensional Array

     This API is implemented for two dimensional array to display the values in clockwise snailshell pattern.
     
Technology Used:

   1.Java 8
   2.SpringBoot
   3.OpenAPI
   
 Post API
 
 	This api accepts two dimensional array as request body and displays the required result
 	
 	Local URL : http://localhost:8080/snail
 	Mapping   : POST
 	Request   : [[1,2,3],[4,5,6],[7,8,9]]
 	
 Open API
 
 	This is api documention of post API implementaion
 	
 	Access URL : http://localhost:8080/swagger-ui.html
 	
 SpringBoot framework use tomcat server which helps to run this application without any server.
 
 Checkout the code and use any of IDE to run this application
 
   1. STS  - https://www.geeksforgeeks.org/how-to-run-your-first-spring-boot-application-in-spring-tool-suite/
   2. Eclipse - https://www.geeksforgeeks.org/how-to-run-your-first-spring-boot-application-in-eclipse-ide/
   3. Intellji - https://www.geeksforgeeks.org/how-to-run-your-first-spring-boot-application-in-intellij-idea/
   
   
 Happy Coding !!!  
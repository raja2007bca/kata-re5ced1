package com.kata.snail.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class SnailService {

	public int[] snailCodewar(int[][] twoDimensionalArray) {

		List<List<Integer>> list = new ArrayList<>();
		list.add(Arrays.stream(twoDimensionalArray[0]).boxed().collect(Collectors.toList()));

		while (twoDimensionalArray != null && twoDimensionalArray.length > 1) {
			twoDimensionalArray = copyNext(twoDimensionalArray);

			if (twoDimensionalArray != null) {
				twoDimensionalArray = displayClockwise(twoDimensionalArray);
				list.add(Arrays.stream(twoDimensionalArray[0]).boxed().collect(Collectors.toList()));
			}
		}
		return list.stream().flatMap(List::stream).mapToInt(i -> i).toArray();

	}

	private int[][] copyNext(int[][] twoDimensionalArray) {

		int[][] newArray = new int[twoDimensionalArray.length - 1][twoDimensionalArray[0].length];

		for (int i = 1; i < twoDimensionalArray.length; i++) {
			for (int j = 0; j < twoDimensionalArray[0].length; j++) {
				newArray[i - 1][j] = twoDimensionalArray[i][j];
			}
		}
		return newArray;
	}

	private int[][] displayClockwise(int[][] array) {

		int[][] finalArray = new int[array[0].length][array.length];

		for (int i = 0; i < array[0].length; i++) {
			for (int j = 0; j < array.length; j++) {
				finalArray[i][j] = array[j][Math.abs(i - (array[0].length - 1))];
			}
		}
		return finalArray;
	}

}

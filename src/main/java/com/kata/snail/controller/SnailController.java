package com.kata.snail.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kata.snail.service.SnailService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
public class SnailController { 

	private SnailService snailService;

	public SnailController(SnailService snailService) {
		this.snailService = snailService;
	}

	@PostMapping(value = "/snail")
	@Operation(summary = "Get client by client request body",
    description = "Kata CodeWar. Purpose of this API is display Snail type implementation for Two Dimentional Array ")
	public ResponseEntity<int[]> snailCodewar(@RequestBody int[][] twoDimensionalArray) {
		return ResponseEntity.ok(snailService.snailCodewar(twoDimensionalArray));
	}
}

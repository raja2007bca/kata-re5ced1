
package com.kata.snail.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SnailControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
		this.mockMvc = builder.build();
	}

	@Test
	public void snailCodewarEmptyArrayTest() throws Exception {

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/snail")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("[[]]");

		this.mockMvc.perform(builder).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));
	}

	@Test
	public void snailCodewarTest() throws Exception {

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/snail")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("[[1,2,3],[4,5,6],[7,8,9]]");

		this.mockMvc.perform(builder).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(9))
				.andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty());
	}

	@Test
	public void snailCodewarNegativeTest() throws Exception {

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/snail")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("");

		this.mockMvc.perform(builder).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
}
